package com.example.todo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/service")
public class TodoController {
	
	@RequestMapping(value="/helloworld", method = RequestMethod.GET)
	public String helloWorld()
	{
		return "Hello world !!!";
	}

}
